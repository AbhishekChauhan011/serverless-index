import json 

def lambda_handler(event, context):
    requestContext = event['requestContext']
    method = requestContext['httpMethod']
    
    if method == "GET":
        return {
            'statusCode': 200,
            'body': json.dumps('User Created Successfully'),
            'headers':{
                'Access-Control-Allow-Origin': '*'
            }
        }
        
    elif method == "POST":
        data = json.loads(event['body'])
        words = data.split(' ') 
        output = ' '.join(reversed(words)) 
        
        return {
            'statusCode': 200,
            'body': json.dumps(output),
            'headers':{
                'Access-Control-Allow-Origin': '*'
            }
        }
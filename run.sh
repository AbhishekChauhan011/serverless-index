#!/usr/bin/env bash
DOMAIN=$(aws cloudformation list-exports --region ap-south-1 --query "Exports[?Name=='sls-firstserverlessproject-dev-ServiceEndpoint'].Value" --output text)
echo Domain:"${DOMAIN}"
DOMAIN="${DOMAIN}/items"
sed -i "s+apiUrl+$DOMAIN+g" index.html
aws s3 cp index.html s3://index-file-bucket/index.html